import json
from DataAccess.conexionMongoDB import Conexion

class OptionSwitcher():

    def main(self, argument):
        method_name = 'option_' + str(argument)
        return getattr(self, method_name)()

    def option_1(self):
        conexion = Conexion()
        mydict = {"nombre":"Adolfo","TelefonoCell":"982932115","TelefonoFijo":"987654321","email":"adolfo@hotmail.com",
        "FechaNacimiento":"17-11-1876","Direccion":""}
        print('Se ha añadido un nuevo contacto')
        conexion.agregarUno(mydict)

    def option_2(self):
        conexion = Conexion()
        conexion.traerLista()
        print('---- se trajo la lista de contactos completa ----')

    def option_3(self):
        
        conexion = Conexion()
        
        
        #valor=myquery  
        x = input("ingrese un nombre de contacto")
        myquery = {"nombre":"Fredy"}
        #conexion.traerData(myquery) 
         
        if myquery == x :
            print ('Uhs el nombre buscado es')
            
        else:
            conexion.traerData(myquery)
            # myquery = {"nombre":"Adolfo"}
            # conexion.traerData(myquery)
        #print('option_3 - buscar')
        #myquery = {'cliente.nroDocCliente': '47295553'}
        #conexion.traerData(myquery)
        
        
        
        # contacto = self.buscarContacto(dni)
        # if contacto is None:
        #     print ('Uhs el contacto buscado no existe')
        # else:
        #     print(json.dumps(contacto, indent=2))

    def option_4(self):
    
        conexion = Conexion()
        myquery = {"nombre":"Fredy"}
         
        newvalues = { "$set": {"nombre": "Fernandinho"} }
        conexion.actualizarData(myquery,newvalues)
        conexion.traerData(myquery)
    def option_5(self):
        conexion = Conexion()
        #print('option_5 - Eliminar')
        myquery = {"nombre":"Federer"}
        
        conexion.borrarData(myquery)
        

        # dni = input('Ingrese el DNI del contacto a buscar : ')
        # resultado = self.eliminarContacto(dni)
        # if resultado is None:
        #     print ('Uhs el contacto que intenta eliminar no existe')
        # else:
        #     print ('El contacto ha sido eliminado correctamente.....!!!! ')
